resource "aws_instance" "this" {
  ami           = var.ami
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
  key_name      = var.key_name

  associate_public_ip_address = true
  monitoring                  = true

  vpc_security_group_ids = var.vpc_security_group_ids

  tags = var.tags
}

resource "aws_eip" "this" {
  count = var.eip_enabled ? 1 : 0
  vpc   = true
}

resource "aws_eip_association" "this" {
  count         = var.eip_enabled ? 1 : 0
  instance_id   = aws_instance.this.id
  allocation_id = aws_eip.this[0].id
}
