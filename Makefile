include .env
export

reinit: 
	rm -rf .terraform.lock.hcl .terraform
	make init

init:
	terraform init -backend-config=config/${ENV}/${ENV}-backend.conf -backend-config="password=${GITLAB_TOKEN}"
	terraform fmt
	terraform validate

plan:
	terraform plan -var-file=config/${ENV}/${ENV}-terraform.tfvars --out plan-${ENV}

apply:
	terraform apply --auto-approve plan-${ENV}

inventory:
	chmod +x inv.sh
	./inv.sh
