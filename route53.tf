locals {
  alb_records = {
    "gogol"       = { domain = "${var.subdomain}${var.domain}" },
    "grafana"     = { domain = "${var.grafana_subdomain}${var.domain}" },
    "monitoring"  = { domain = "${var.monitoring_subdomain}${var.domain}" },
    "server-node" = { domain = "${var.server_node_subdomain}${var.domain}" }
  }
}

resource "aws_route53_record" "alb_records" {
  for_each = local.alb_records

  zone_id = var.zone_id
  name    = each.value.domain
  type    = "A"

  alias {
    name                   = module.alb.lb_dns_name
    zone_id                = module.alb.lb_zone_id
    evaluate_target_health = true
  }
}
