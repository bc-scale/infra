region = "us-east-1"

domain                = "staboss.com"
subdomain             = ""
grafana_subdomain     = "grafana."
monitoring_subdomain  = "monitoring."
server_node_subdomain = "server-node."

private_subnet_count = 2
public_subnet_count  = 2
vpc_cidr_block       = "10.0.0.0/16"

key_name    = "prod"
environment = "prod"

tags = {
  Terraform   = "true"
  Environment = "prod"
}

zone_id = "Z01023043EM9MP9SXVE3Z"
